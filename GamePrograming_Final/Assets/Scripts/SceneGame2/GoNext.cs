﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoNext : MonoBehaviour
{
    public void GoNextScene()
    {
        SceneManager.LoadScene("Game2");
    }
}
